package main
import (
    "github.com/roowe/gofin"
    //"bytes"
    "fmt"
    "os"
    "encoding/csv"
    "log"
    "io"
    "strconv"
)

type longwinConf struct{
    fundCode string
    trackIndex string
    inner string
}
type GuZhi struct{
    fundCode string
    gz *gofin.FundGuZhi
    trackIndex *gofin.HSStockData
    inner *gofin.HSStockData
}
func GetLastPrice(code string) (*gofin.HSStockData, error) {
    if code == "" || code == " " {
        return nil, nil
    } else {
        return gofin.GetLastPrice(code)
    }
}

func GetGZData() {
    confs := loadLongwinConf()
    guzhis := make([]GuZhi, 0)
    for _, conf := range confs {
        fundGZ, err1 := gofin.GetGuZhi(conf.fundCode)                
        trackIndexData, err2 := GetLastPrice(conf.trackIndex)
        innerData, err3 := GetLastPrice(conf.inner)
        gz := GuZhi{
            fundCode: conf.fundCode,
        }
        if err1 == nil && err2 == nil && err3 == nil {
            gz.gz = fundGZ
            gz.trackIndex = trackIndexData
            gz.inner = innerData
        } else {
            fmt.Println(conf, err1, err2, err3)
        }
        guzhis = append(guzhis, gz)
    }
    SaveGZ(guzhis)    
}
func SaveGZ(gzs []GuZhi) {    
    fout, err := os.Create(gzFile)
    if err != nil {
        log.Fatal(err)
    }
    defer fout.Close()
    r := csv.NewWriter(fout)
    fields := []string{
        "证券码", // [0]
        "最新净值", // [1]
        "截止日期", // [2]
        "ttj估算价格", // [3]
        "ttj估算涨跌%", // [4]
        "ttj估算时间", // [5]
        "指数名字", // [6]
        "指数价格", // [7]
        "指数涨跌%", // [8]
        "场内证券码", // [9]
        "场内价格", // [10]
        "场内涨跌%", // [11]
    }
    r.Write(fields)
    for _, gz := range gzs {
        s := make([]string, 12)
        s[0] = gz.fundCode
        if gz.gz != nil {
            s[1] = gz.gz.DWJZ
            s[2] = gz.gz.JZRQ
            s[3] = gz.gz.GSZ
            s[4] = gz.gz.GSZZL
            s[5] = gz.gz.GZTIME
        }
        if gz.trackIndex != nil {
            s[6] = gz.trackIndex.Name
            s[7] = strconv.FormatFloat(gz.trackIndex.NowPri, 'g', -1, 64)
            s[8] = strconv.FormatFloat(gz.trackIndex.ChangePer, 'g', -1, 64)
        }
        if gz.inner != nil {
            s[9] = gz.inner.Gid
            s[10] = strconv.FormatFloat(gz.inner.NowPri, 'g', -1, 64)
            s[11] = strconv.FormatFloat(gz.inner.ChangePer, 'g', -1, 64)
        }        
        r.Write(s)
    }
    r.Flush()
}

func loadLongwinConf() []longwinConf {
    // data/longwinConf.csv
    fileConf, err := os.Open(confFile)
    defer fileConf.Close()
    r := csv.NewReader(fileConf)
    fields, err := r.Read()    
    if err != nil {
        log.Fatal(err)
    }
    fieldLen := len(fields)
    confs := make([]longwinConf, 0)
	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
        conf := longwinConf{}
        for i:=0; i<fieldLen; i++ {
            switch fields[i]{
            case "fundCode":
                conf.fundCode = record[i]
            case "trackIndex":
                conf.trackIndex = record[i]
            case "inner":
                conf.inner = record[i]
            }
        }
        confs = append(confs, conf)
	}
    return confs
}
