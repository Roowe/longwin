package main
import (
    //"fmt"
    "encoding/json"
    "io/ioutil" 
    "log"
    "encoding/csv"
    "os"
    "bytes"
    "time"
    "strconv"
)

type History struct {
    FundCode string 
    Nav float64 `json:"nav"` // 确认价格 
    NavDate int64 `json:"navDate"` // 确认日期
    TradeUnit int `json:"tradeUnit"` // 份数
    OrderCode string `json:"orderCode"` // "022" 买入 "024" 卖出
}


func getPlan() {
    content, err := ioutil.ReadFile("data/plan.json")
    if err != nil {
        log.Fatal(err)
    }
    planData := make(map[string]json.RawMessage)
    json.Unmarshal(content, &planData)
    coms := make([]map[string]json.RawMessage, 0)
    json.Unmarshal(planData["composition"], &coms)
    logs := make([]History, 0)
    for _, com := range(coms) {
        if rawFund, found := com["fund"]; found {
            fund := make(map[string]interface{})
            json.Unmarshal(rawFund, &fund)
            logs = append(logs, GetHistory(fund["fundCode"].(string))...)
        }
    }
    SaveCSV(logs)
}
const FIELD_LEN = 5
func SaveCSV(logs []History) {
    buf := new(bytes.Buffer)
    r2 := csv.NewWriter(buf)
    fields := []string{"fundCode","navDate","tradeUnit","nav","orderCode"}
    r2.Write(fields)
    for _, h := range(logs) {
        s := make([]string, FIELD_LEN)
        s[0] = h.FundCode
        tm := time.Unix(h.NavDate/1000, 0)        
        s[1] = string(tm.Format("2006-01-02"))
        s[2] = strconv.Itoa(h.TradeUnit)
        s[3] = strconv.FormatFloat(h.Nav, 'g', -1, 32)
        s[4] = h.OrderCode        
        r2.Write(s)
    }
    r2.Flush()
    fout,err := os.Create("data/history.csv")
    defer fout.Close()
    if err != nil {
        log.Fatal(err)
    }
    fout.WriteString(buf.String())
}

func GetHistory(fundCode string) []History {
    content, err := ioutil.ReadFile("data/composition/" + fundCode)
    if err != nil {
        log.Fatal(err)
    }
    logs := make([]History, 0)
    json.Unmarshal(content, &logs)
    n := len(logs)
    for i:=0; i<n; i++ {
        logs[i].FundCode=fundCode
    }
    return logs
}
