package main
// import (
//     "fmt"
//     "github.com/buger/jsonparser"
//     "io/ioutil"
// 	"time"
// 	"strconv"
//     "crypto/sha256"
//     "strings"
//     "encoding/hex"
//     "net/http"
//     "math/rand"
//     "errors"
// )

// const (
//     MaxRetry = 10
// )

// func test() {
//     fmt.Println("hello test")
// }

// type qieman struct {
    
// }

// type longwin struct {
// 	funds map[string] fund
// }


// type fund struct {
//     fundCode string
//     fundName string
//     nav float64
//     unitValue float64
//     planUnit int64
//     faChes  []faCheLog
// }

// type faCheLog struct {
//     adjustmentID int64 // 自增id
//     nav float64 // 确认价格
//     navDate int64 // 确认日期
//     tradeUnit int64 // 份数
//     orderCode string // "022" 买入 "024" 卖出
// }

// func (qm qieman) FetchFaCheLog(fund *fund)  error{
//     content := qm.FaCheLog(fund.fundCode)

//     if len(content) == 0{
//         return errors.New("获取不到发车数据")
//     }
//     fund.faChes = make([]faCheLog, 0)    
//     jsonparser.ArrayEach(content,
//         func(faCheLogJSON []byte, dataType jsonparser.ValueType, offset int, err error) {
//             if err == nil {                                    
//                 adjustmentID, _ := jsonparser.GetInt(faCheLogJSON, "adjustmentId")  
//                 orderCode, _ := jsonparser.GetString(faCheLogJSON, "orderCode")  
//                 nav, _ := jsonparser.GetFloat(faCheLogJSON, "nav")  
//                 navDate, _ := jsonparser.GetInt(faCheLogJSON, "navDate")                  
//                 tradeUnit, _ := jsonparser.GetInt(faCheLogJSON, "tradeUnit")  
                
//                 fund.faChes = append(fund.faChes, faCheLog{
//                     adjustmentID: adjustmentID,
//                     orderCode: orderCode,
//                     nav: nav,
//                     navDate: navDate,
//                     tradeUnit: tradeUnit, 
//                 })                               
//             }
//         })    
//     return nil
// }

// func (qm qieman) FetchPlan() (longwin, error){
//     lw := longwin{}
//     content := qm.LongWinPlan()
    
//     if len(content) == 0{
//         return lw, errors.New("计划数据为空")
//     }

//     lw.funds = make(map[string]fund)
//     jsonparser.ArrayEach(content,
//         func(fundJSON []byte, dataType jsonparser.ValueType, offset int, err error) {
//             if err == nil {                    
//                 fundCode, err := jsonparser.GetString(fundJSON, "fund", "fundCode")
//                 if err == nil {
//                     fundName, _ := jsonparser.GetString(fundJSON, "fund", "fundName")  
//                     nav, _ := jsonparser.GetFloat(fundJSON, "nav")
//                     unitValue, _ := jsonparser.GetFloat(fundJSON, "unitValue")
//                     planUnit, _ := jsonparser.GetInt(fundJSON, "planUnit")
//                     fund := fund{
//                         fundCode: fundCode,
//                         fundName: fundName,
//                         nav: nav,
//                         unitValue: unitValue,
//                         planUnit: planUnit, 
//                     }
//                     err := qm.FetchFaCheLog(&fund)
//                     if err == nil {
//                         lw.funds[fundCode] = fund
//                     } else {
//                         fmt.Printf("fachelog err %s\n", err)
//                     }
//                 }
//             }
//         }, "composition")    
//     return lw,nil
// }

// func (qm qieman) xSign() string {
//     timestamp := time.Now().UnixNano()
//     hashByte := []byte(strconv.Itoa(int(float64(timestamp)*1.01))[:13])

//     md := sha256.Sum256(hashByte)

//     signStr := strings.ToUpper(hex.EncodeToString(md[:16]))
//     ret := strconv.Itoa(int(timestamp/1000000)) + signStr
//     // fmt.Println(timestamp, ret)
//     return ret   
// }
// func (qm qieman) xReqId() string {
//     token := make([]byte, 10)
//     rand.Read(token)
//     reqId := strings.ToUpper(hex.EncodeToString(token))
//     return reqId
// }
// func (qm qieman) reliableReq(Url string, Ref string) []byte {
//     for i:=0; i<MaxRetry; i++ {
//         content := qm.req(Url, Ref)
//         if len(content) > 0{
//             return content
//         } else {
//             fmt.Printf("%s retry %d times\n", Url, i)
//         }
//     }
//     return []byte("")
// }

// func (qm qieman) req(Url string, Ref string) []byte {
//     client := &http.Client{}
//     // 
//     req, err := http.NewRequest("GET", Url, nil)
//     if err != nil {
//         return []byte("")
//     }
//     req.Header.Set("Accept", "application/json")
//     //req.Header.Set("Accept-Encoding", "gzip, deflate, br")
//     //req.Header.Set("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.6,en;q=0.4")
//     //req.Header.Set("Connection", "keep-alive")
    
//     req.Header.Set("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36")
//     req.Header.Set("x-sign", qm.xSign())
//     req.Header.Set("Host", "qieman.com")
//     req.Header.Set("Referer", Ref)
//     req.Header.Set("x-request-id", qm.xReqId())
//     resp, err := client.Do(req)
//     content, err := ioutil.ReadAll(resp.Body)
//     //fmt.Printf("%s\n", content)
// 	resp.Body.Close()
//     return content
// }

// func (qm qieman) LongWinPlan() []byte {
//     return qm.reliableReq("https://qieman.com/pmdj/v2/long-win/plan",
//         "https://qieman.com/longwin/index")
// }

// func (qm qieman) FaCheLog(fundCode string) []byte {
//     return qm.reliableReq("https://qieman.com/pmdj/v2/long-win/plan/history?fundCode="+fundCode,
//         "https://qieman.com/longwin/funds/"+fundCode)
// }

