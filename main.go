package main

import (
    "fmt"
    "flag"
)

var confFile string
var gzFile string

func main() {
    flag.StringVar(&confFile, "conf", "longwinConf.csv", "longwin conf")
    flag.StringVar(&gzFile, "gz", "gz.csv", "guzhi file")
    flag.Parse()
    fmt.Println("start get guzhi data")
    GetGZData()
}
